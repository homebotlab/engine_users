const express = require('express');

const aclMiddleware = require(`${process.env.MIDDLEWARES_PATH}/permission`);
const authMiddleware = require(`${process.env.MIDDLEWARES_PATH}/authentication`);

const users = require('../controllers/user');

const routes  = express.Router();

routes.route('/')
  .all(authMiddleware())
  .get(users.list)
  .post(users.create);

routes.route('/:id')
  .all(authMiddleware())
  .get(users.read)
  .put(users.update)
  .delete(users.delete);

routes.route('/notificate')
  .post(users.notificate);

routes.route('/:id/notifications')
  .all(authMiddleware())
  .get(users.notifications);

module.exports = routes;
