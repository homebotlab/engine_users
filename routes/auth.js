const express = require('express');
const passport = require('passport');
const auth = require('../controllers/auth');
const authMiddleware = require(`${process.env.MIDDLEWARES_PATH}/authentication`);
const routes = express.Router();

routes.route('/register')
  .post(auth.register);

routes.route('/logout')
  .get(auth.logout);

routes.route('/profile')
  .all(authMiddleware())
  .get(auth.profile);

routes.route('/approve-email')
  .get(auth.approveEmail);

routes.route('/recovery-password')
  .post(auth.recoveryPass);

routes.route('/change-password')
  .post(auth.changePass);
  
// Local Auth

routes.route('/login')
  .post(passport.authenticate('local', {
    failureRedirect: '/login',
    failureFlash: true
  }), auth.login);

function fnRedirect(req, res){

    let target = '/modules/launchpad/webapp';
    switch (req.user.role.name) {
      case 'admin': {
        target = '/modules/launchpad/webapp';
        break;
      };
    }

    res.redirect(target);
}

// VK Auth

routes.route('/vk')
  .get(passport.authenticate('vkontakte', { scope: ['email'] }));

routes.route('/vk/callback')
  .get(passport.authenticate('vkontakte', {
    failureRedirect: '/login'
  }), fnRedirect);

// FB Auth

routes.route('/fb')
  .get(passport.authenticate('facebook', { scope: ['email'] }));

routes.route('/fb/callback')
  .get(passport.authenticate('facebook', {
    failureRedirect: '/login'
  }), fnRedirect);

// GOOGLE Auth

routes.route('/google')
  .get(passport.authenticate('google', { scope: ['email'] }));

routes.route('/google/callback')
  .get(passport.authenticate('google', {
    failureRedirect: '/login'
  }), fnRedirect);

module.exports = routes;