const mongoose = require('mongoose');
const response = require(`${process.env.HELPERS_PATH}/response`);
const request = require(`${process.env.HELPERS_PATH}/request`);
const pagination = require(`${process.env.HELPERS_PATH}/pagination`);
const acl = require(`${process.env.HELPERS_PATH}/acl`);
const bcrypt = require('bcryptjs');
const log = require('log4js').getLogger("user");

require("../models/user");
const User = mongoose.model('User');

exports.list = async (req, res) => {
  try {
    const users = await User.paginate(
      request.getFilteringOptions(req), {
        ...request.getRequestOptions(req)
      });

    const allowedUsers = [];
    const role = req.user.role;

    users.docs.forEach(user => {
      const permission = (req.user._id == user._id) ?
        acl.can(role).readOwn('user') :
        acl.can(role).readAny('user');

      if (permission.granted) {
        const filteredUsers = permission.filter(JSON.parse(JSON.stringify(user)));
        allowedUsers.push(filteredUsers);
      }
    });

    pagination.setPaginationHeaders(res, users);
    res.json(allowedUsers);

  } catch (err) {
    response.sendBadRequest(res, err);
  }
};

exports.read = async (req, res) => {
  try {
    const user = await User.findById(req.params.id)
      .populate('regions')
      .exec();

    const role = req.user.role;
    const permission = (user._id == user._id) ?
      acl.can(role).readOwn('user') :
      acl.can(role).readAny('user');

    if (permission.granted) {
      const filteredData = permission.filter(JSON.parse(JSON.stringify(user)));
      return res.json(filteredData);
    }

    return response.sendForbidden(res);

  } catch (err) {
    response.sendBadRequest(res, err);
  }
};

exports.create = async (req, res) => {
  try {
    const user = new User({ ...req.body });
    const newUser = await User.register(user, req.body.password);
    this.info(`Created user with email ${user.email}.`);

    return response.sendCreated(res, newUser);
  } catch (err) {
    this.err('Error cheating new user', err);
    response.sendBadRequest(res, err);
  };
};

exports.update = async function (req, res) {
  try {
    const data = req.body;
    const userId = req.params.id;
    const user = await User.findById(userId).exec();

    if (!user) return response.sendNotFound(res);

    const role = req.user.role;
    const permission = (req.user._id == user._id) ?
      acl.can(role).updateOwn('user') :
      acl.can(role).updateAny('user');

    if (permission.granted) {
      const userData = permission.filter(JSON.parse(JSON.stringify(data)));

      Object.assign(user, userData);
      const updatedUser = await user.save();
      log.info(`User ${user.email} updated own data`);

      return res.json(updatedUser);
    }
    log.warn(`Forbidden updating user ${user.email} data`);
    response.sendForbidden(res);

  } catch (err) {
    this.err('Error updating user data', err);
    response.sendBadRequest(res, err);
  }
}

exports.delete = async (req, res) => {
  try {
    const role = req.user.role;
    const permission = (req.user._id == req.params.id) ?
      acl.can(role).deleteOwn('user') :
      acl.can(role).deleteAny('user');

    if (permission.granted) {
      await User.remove({
        _id: req.params.id
      }).exec();

      log.info(`User with id ${req.params.id} has been deleted`);
      return res.json({
        message: 'User successfully deleted'
      });
    }
    log.warn(`Forbidden deletining user ${user.email}`);
    response.sendForbidden(res);

  } catch (err) {
    response.sendBadRequest(res, err);
  }
};

exports.notifications = async function (req, res) {
  const user = await User.findById(req.params.id)
    .populate('notifications');

  res.json(user.notifications);
}

exports.notificate = async function (req, res) {
  const {
    user,
    body: {
      receiverIds,
      description,
      type,
      title
    }
  } = req;

  const notification = await Notification.create({
    sender: user._id,
    receivers: receiverIds,
    description,
    type,
    title
  });

  res.json(notification);
}