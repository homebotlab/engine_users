const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const url = require('url');
const log = require('log4js').getLogger("user");

const mailer = require('../../../mailer');
const publicUrl = process.env.PUBLIC_URL;

const User = mongoose.model('User');

exports.register = async function (req, res) {
  try {
    const userData = Object.assign({ ...req.body
    }, {
      status: false,
      role: 'user',
      regions: [req.body.region]
    });

    await User.register(new User({ ...userData
    }), req.body.password);
    log.info(`New user with email ${userData.email} registered`);

    // send notification to admin
    await mailer.sendMail({
      from: process.env.EMAIL_USER,
      to: process.env.ADMIN_EMAIL,
      subject: 'Зарегистрирован новый пользователь',
      template: 'engine_users/email/newUser',
      context: {
        firstname: userData.firstname,
        middlename: userData.middlename,
        lastname: userData.lastname
      }
    });
    log.info(`Sended the email notification to ${process.env.ADMIN_EMAIL} new user with email ${userData.email}`);

    req.flash('success', true);
    res.redirect('/register');

  } catch (errors) {
    log.error('Registration error', errors);
    req.flash('error', errors);
    res.redirect('/register');
  }
}

exports.login = function (req, res) {
  req.login(req.user, (err) => {
    let target = "/modules/launchpad/webapp/index.html";

    if (err) {
      log.warn(`Login failed for user with email ${req.user ? req.user.email: ''}`);
      req.flash('error', err);
      res.redirect('/login');
    }

    if (req.session.target) {
      target = req.session.target;
      req.session.target = null;
    }

    log.info(`Successfully login. User ${req.user ? req.user.email: ''}`);
    res.redirect(target);
  });
}

exports.approveEmail = async function (req, res) {
  try {
    const {
      email,
      hash
    } = req.query;

    const user = await User.findOne({
      email
    }).exec();

    if (!user) {
      log.warn(`Approve email attempt for non existing user with email ${email}`);
      req.flash('error', { message: 'User not found' });
      return res.redirect('/approve-email');
    }

    const userHashData = user.email + user.password;
    if (!bcrypt.compareSync(userHashData, hash)) {
      log.warn(`Invalid hash approving email. User ${email}`);
      req.flash('error', { message: 'Invalid hash' });
      return res.redirect('/approve-email');

    } else {

      user.isEmailApproved = true;
      user.save();
      log.info(`User with email ${email} successfully approved email`);

      req.flash('success', true);
      res.redirect('/approve-email');
    }

  } catch (err) {
    log.error('Approve email error', err);
    req.flash('error', { message: err });
    return res.redirect('/approve-email');
  }
}

exports.logout = function (req, res) {
  req.logout();
  req.session.destroy();
  log.info(`User ${req.user ? req.user.email: ''} logged out`);
  res.redirect("/");
}

exports.profile = async function (req, res) {
  const user = await User.findById(req.user._id)
    .populate('regions')
    .select('-password');
  res.json({ ...user._doc });
}

exports.recoveryPass = async function (req, res) {
  const {
    email,
    captcha
  } = req.body;
  const user = await User.findOne({
    email
  }).exec();

  if (captcha.toLowerCase() !== req.session.captcha.toLowerCase()) {
    log.warn(`Error in captcha when recovering password. User ${email}`);
    req.flash('error', {  message: 'Ошибка в капче' });
    return res.redirect('/recovery-password');
  }

  if (!user) {
    log.warn(`User is not found when recovering password. User ${email}`);
    req.flash('error', { message: 'Пользователь с указанным email не найден' });
    return res.redirect('/recovery-password');
  }

  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(user.email + user.password, salt);

  mailer.sendMail({
    from: process.env.EMAIL_USER,
    to: email,
    subject: 'Восстановление пароля',
    template: 'engine_users/email/recoveryPass',
    context: {
      recoveryUrl: `${publicUrl}/change-password?email=${email}&hash=${hash}`
    }
  });
  log.info(`Sended email for recovering password. User ${email}`);

  req.flash('success', true);
  return res.redirect('/recovery-password');
}

exports.changePass = async function (req, res) {
  const { password, repeated_password } = req.body;
  const refererData = url.parse(req.headers.referer, true);
  const {
    query: {
      email,
      hash
    }
  } = refererData;
  const search = refererData.search;

  try {

    if (password !== repeated_password) {
      log.warn(`Passwords don't match when changing password. User ${email}`);
      req.flash('error', { message: 'Пароли не совпадают' });
      return res.redirect(`/change-password/${search}`);
    }

    if (!email || !hash) {
      log.warn(`Email and Hash is not found when changing password. User ${email}`);
      req.flash('error', { message: 'Unexpected error' });
      return res.redirect(`/change-password/${search}`);
    }

    const user = await User.findOne({
      email
    }).exec();

    if (!user) {
      log.warn(`User with email ${email} is not found when changing password`);
      req.flash('error', { message: 'Unexpected error' });
      return res.redirect(`/change-password/${search}`);
    }

    const userHashData = user.email + user.password;

    if (!bcrypt.compareSync(userHashData, hash)) {
      log.warn(`Invalid hash when changing password. User ${email}`);
      req.flash('error', { message: 'Invalid hash' });
      return res.redirect(`/change-password/${search}`);

    } else {
      user.password = password;
      user.save();

      log.warn(`User with email ${email} successfully changed password`);
      req.flash('success', true);
      res.redirect(`/change-password/${search}`);
    }

  } catch (err) {
    log.error('Unexpected error when changing password', err);
    req.flash('error', { message: err });
    return res.redirect(`/change-password/${search}`);
  }
}