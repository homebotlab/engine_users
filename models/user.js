const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const passportLocalMongoose = require('passport-local-mongoose');
const bcrypt = require('bcryptjs');
const log = require('log4js').getLogger("user");
const mailer = require('../../../mailer');
const publicUrl = process.env.PUBLIC_URL;

const Schema = mongoose.Schema;
const UserSchema = new Schema({
  avatar: {
    type: String
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  username: {
    type: String
  },
  password: {
    type: String,
    required: true
  },
  firstname: {
    type: String
  },
  lastname: {
    type: String
  },
  middlename: {
    type: String
  },
  status: Boolean,
  isEmailApproved: {
    type: Boolean,
    default: false
  },
  role: {
    type: String,
    default: 'user'
  },
  regions: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Region'
  }],
  dob: {
    type: Date
  },
  gender: {
    type: String,
    enum: ['female', 'male']
  },
  city: String,
  phone: String
}, {
  timestamps: {
    createdAt: 'createdAt'
  },
  toJSON: {
    virtuals: true
  }
});

UserSchema.set('toJSON', {
  transform: function (doc, ret, options) {
    delete ret.password;
    return ret;
  }
});

UserSchema.virtual('organizator', {
  ref: 'Event',
  localField: '_id',
  foreignField: 'organizator',
  justOne: false
});

UserSchema.virtual('files', {
  ref: 'File',
  localField: '_id',
  foreignField: 'owner',
  justOne: false
});

UserSchema.pre('save', function (next) {

  if (this.isModified('password')) {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(this.password, salt);
    this.password = hash;
    log.info(`User ${this.email} has changed password`);
  }
  
  if (this.isModified('email')) {
    const { email, password } = this;
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(email + password, salt);
    log.info(`User ${this.email} has changed email`);

    mailer.sendMail({
      from: process.env.EMAIL_USER,
      to: email,
      subject: 'Подтверждение регистрации',
      template: 'engine_users/email/approveEmail',
      context: {
        url: `${publicUrl}/api/v0.1/auth/approve-email?email=${email}&hash=${hash}`
      }
    }); 
    log.info(`Sended email to approve email for user ${email}`);

    this.isEmailApproved = false;
    log.info(`User ${this.email} has changed isEmailApproved=false`);
  }

  if (this.isModified('status') && !this.isNew) {
    const { email, status } = this;

    mailer.sendMail({
      from: process.env.EMAIL_USER,
      to: email,
      subject: 'Информирование о статусе аккаунта',
      template: 'engine_users/email/changedStatus',
      context: {
        status: status ? 'активирован': 'деактивирован'
      }
    });
    log.info(`Sended email to notificate about new status for user ${email}`);
    log.info(`User ${this.email} has changed status. Now status is ${status}`);    
  }  

  return next();
});

['findOne', 'findOneAndUpdate'].forEach(function (hook) {
  UserSchema.pre(hook, function () {
    this.populate('files');
  });
});

UserSchema.methods.equals = function (user) {
  return this._id == user._id;
};

UserSchema.plugin(mongoosePaginate);
UserSchema.plugin(passportLocalMongoose, {
  usernameField: 'email',    
  errorMessages: {
    MissingPasswordError: 'Вы не указали пароль',
    MissingUsernameError: 'Вы не указали email',
    UserExistsError: 'Пользователь с таким email уже зарегистрирован'
  }
});

module.exports = mongoose.model('User', UserSchema);